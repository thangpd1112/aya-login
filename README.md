## The python script that will automatically login to aya ibanking and passby captcha

# How to run

- Run

```py
pip install -r requirements.txt
```

to install all pip dependencies list in requirements.txt

- Install tesseract at https://github.com/UB-Mannheim/tesseract/wiki

- In file handler.py, update the following line to your tesseract execution path:

```python
pytesseract.pytesseract.tesseract_cmd = '<full_path_to_your_tesseract_executable>' # Include the above line, if you don't have tesseract executable in your PATH
```

- From terminal, run py main.py
