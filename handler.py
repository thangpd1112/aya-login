import sys
import time
from dotenv import dotenv_values
from typing import Callable
from io import BytesIO
from PIL import Image
import pytesseract
from selenium.webdriver import Chrome, ChromeOptions
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By

# pytesseract.pytesseract.tesseract_cmd = '<full_path_to_your_tesseract_executable>' 
# Include the above line, if you don't have tesseract executable in your PATH

WAIT_TIMEOUT_IN_SECONDS = 10
AYA_LANDING_PAGE = 'https://www.ayaibanking.com'

USERNAME_INPUT_ID = "txtUser"
PASSWORD_INPUT_ID = "txtPass"
CAPTCHA_IMAGE_ID = "Image1"
CAPTCHA_INPUT_ID = "txtValidateCode"
LOGIN_BTN_ID = "btnLogin2"

def handler():
	config = dotenv_values('.env')
	aya_username = config.get("AYA_USERNAME")
	aya_password = config.get("AYA_PASSWORD")
	account = {
		'username': aya_username,
		'password': aya_password
	}

	executable_path = './chrome/chromedriver.exe'
	options = ChromeOptions()
	options.add_argument("--window-size=1300,600")
	driver = Chrome(executable_path=executable_path, options=options)
	scraper_handler(driver, account)

def scraper_handler(driver: Chrome, account):
	wait = WebDriverWait(driver, WAIT_TIMEOUT_IN_SECONDS)
	driver.get(AYA_LANDING_PAGE)

	try:
		username_input = wait_until(wait, lambda d: d.find_element(By.ID, USERNAME_INPUT_ID))
		delayed_send_keys(username_input, account['username'])

		password_input = wait_until(wait, lambda d: d.find_element(By.ID, PASSWORD_INPUT_ID))
		delayed_send_keys(password_input, account['password'])

		captcha_image_element = wait_until(wait, lambda d: d.find_element(By.ID, CAPTCHA_IMAGE_ID))
		captcha_image = captcha_image_element.screenshot_as_png

		i = Image.open(BytesIO(captcha_image))

		decoded_captcha_data = pytesseract.image_to_string(i)

		print("decoded captcha ", decoded_captcha_data)

		captcha_input_element = wait_until(wait, lambda d: d.find_element(By.ID, CAPTCHA_INPUT_ID))
		delayed_send_keys(captcha_input_element, decoded_captcha_data)

		login_btn = wait_until(wait, lambda d: d.find_element(By.ID, LOGIN_BTN_ID))
		login_btn.click()

		time.sleep(10)
		pass

	except NoSuchElementException as err:
		print('no such element error:', err)
		sys.exit(1)

	except Exception as err:
		print('exception orcur:', err)
		sys.exit(1)

	finally:
		print("all done.")
		sys.exit(0)

def wait_until(wait: WebDriverWait, func: Callable[[Chrome], WebElement]):
	return wait.until(func)

def delayed_send_keys(element: WebElement, keys: str, cpm: int = 600):
	each_char_delay_time = 1 / (cpm / 60)
	for key in keys:
		element.send_keys(key)
		time.sleep(each_char_delay_time)