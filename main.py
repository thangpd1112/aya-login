from dotenv import load_dotenv
from handler import handler
load_dotenv()

if __name__ == '__main__':
	handler()